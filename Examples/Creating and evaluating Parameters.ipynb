{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parametric Model\n",
    "This notebook covers how to define a parametric eppy model, and get it's energy use for different parameter values.\n",
    "### Various Imports\n",
    "`EvaluatorEP` manages energy-plus simulations for a single problem.  \n",
    "Problems, like `EPProblem` organise several parameters together.  \n",
    "Parameters, like `Parameter` describe a single variable for the building.\n",
    "They are composed of a `Descriptor` and a `Selector`.\n",
    "`RangeParameter` is a descriptor that indicates a parameter that can take on values from an interval.  \n",
    "`CategoryParameter` is a descriptor that indicates a paramter that can take on values from a list.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from besos import eppy_funcs as ef\n",
    "from besos import sampling\n",
    "from besos.evaluator import EvaluatorEP\n",
    "from besos.parameters import RangeParameter, CategoryParameter, expand_plist, FieldSelector, Parameter, wwr\n",
    "from besos.problem import EPProblem\n",
    "\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each building is described by an `.idf` or `epJSON` file. In order to modify it programatically, we load it as a python object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# by default this will get an example building specifed in the config file.\n",
    "building = ef.get_building()\n",
    "# we can specify our own files too.\n",
    "# (changing the idd file can cause issues, only use one per script/notebook)\n",
    "# when in idf mode the arguments are building=idf file, data_dict=idd file\n",
    "# when in json mode the arguments are "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Eppy's documentation](https://eppy.readthedocs.io/en/latest/) describes how to explore and modify the idf object. If you are using the newer JSON format, then any JSON parsing library will work.  \n",
    "This example uses the fact that the example building contains the following items:\n",
    " - Various `Material` class objects with the \n",
    "   - `Mass NonRes Wall Insulation` (having a `Thickness` field)\n",
    "   - `8IN CONCRETE HW` and\n",
    "   - `HW CONCRETE` \n",
    " - A `Construction` class object called `ext-slab` with an `Outside Layer` field\n",
    " - A `WindowMaterial:SimpleGlazingSystem` class object with `UFactor` and `Solar Heat Gain Coefficient` fields\n",
    " - Multiple `Lights` class objects, all of which have a `Watts per Zone Floor Area` field.\n",
    "\n",
    " The names class, object and field refer to the 3 layers of nesting present in energyplus building description."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parameters\n",
    "Each way in which the building can be modified is represented by a single parameter. These can be created in various ways, and can be used to automatically modify the idf, as well as to optimise it's design.\n",
    "### Making Parameters\n",
    "Parameters are composed of a `Selector` and a `Descriptor`. They can also optionally have a name.  \n",
    "There are notebooks which go into more detail on each of these.\n",
    "#### Selectors\n",
    "Selectors identify which part of the building to modify, and how to modify it. The most common type of selector is a `FieldSelector`. `FieldSelectors` can be created by specifying the class, object, and field names that they apply to.  \n",
    "\n",
    "The example building loaded above contains a `Material` class object named `Mass NonRes Wall Insulation` which has a `Thickness` field. Below is how to make a selector that modifies this insulation's thickness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "insulation = FieldSelector(class_name='Material', \n",
    "                           object_name='Mass NonRes Wall Insulation',\n",
    "                           field_name='Thickness')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Descriptors\n",
    "Descriptors specify what kinds of values are valid for a parameter. If we want to vary a parameter from zero to one excluding the endpoints, we can use a `RangeParameter`, with the apropriate minimum and maximum. (Note that values like 0.001 are also excluded by this example.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "zero_to_one_exclusive = RangeParameter(min_val = 0.01, max_val=0.99)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Parameter(name='Insulation Thickness', selector=FieldSelector(field_name='Thickness', class_name='Material', object_name='Mass NonRes Wall Insulation'), value_descriptor=RangeParameter(min=0.01, max=0.99))\n"
     ]
    }
   ],
   "source": [
    "insulation_param = Parameter(selector=insulation,\n",
    "                                 value_descriptor=zero_to_one_exclusive,\n",
    "                                 name='Insulation Thickness')\n",
    "print(insulation_param)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes you might want to specify several `RangeParameter`s at once. The `expand_plist` funcion can do this more concisely. It takes a nested dictionary as input.  \n",
    "The keys in the first layer of this dictionary are the names of the idf objects to make parameters for.  \n",
    "These are associated with a dictionary that has keys matching the Fields of that object which should be modified. Each field-key corresponds to a tuple containing the minimum and maximum values for that parameter.\n",
    "The `class_name` is not specified. Instead the building is searched for objects with the correct `object_name`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Parameter(name='U-Factor', selector=FieldSelector(field_name='U-Factor', object_name='NonRes Fixed Assembly Window'), value_descriptor=RangeParameter(min=0.1, max=5))\n",
      "Parameter(name='Solar Heat Gain Coefficient', selector=FieldSelector(field_name='Solar Heat Gain Coefficient', object_name='NonRes Fixed Assembly Window'), value_descriptor=RangeParameter(min=0.01, max=0.99))\n"
     ]
    }
   ],
   "source": [
    "more_parameters = expand_plist(\n",
    "    # class_name is NOT provided\n",
    "    #{'object_name':\n",
    "    # {'field_name':(min, max)}}\n",
    "    {'NonRes Fixed Assembly Window':\n",
    "     {'U-Factor':(0.1,5),\n",
    "      'Solar Heat Gain Coefficient':(0.01,0.99)\n",
    "     }\n",
    "    })\n",
    "\n",
    "for p in more_parameters:\n",
    "    print(p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "BESOS also includes some premade parameters, such as window to wall ratio. These can be customised, but for now we will just use the defaults."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Parameter(name='Window to Wall Ratio', selector=GenericSelector(set=<function wwr.<locals>.set at 0x00000165A633A620>, get=<function wwr.<locals>.get at 0x00000165A3257048>, setup=<function wwr.<locals>.setup at 0x00000165A3257840>), value_descriptor=RangeParameter(min=0.01, max=0.99))\n"
     ]
    }
   ],
   "source": [
    "# use a special shortcut to get the window-to-wall parameter\n",
    "window_to_wall = wwr()\n",
    "print(window_to_wall)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parameters can also be created by defining a function that takes an idf and a value and mutates the idf. These functions can be specific to a certain idf's format, and can perform any arbitrary transformation. Creating these can be more involved, and is not covered in this example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problems\n",
    "problem objects represent the inputs and outputs that we are interested in. We have defined the inputs using parameters above, and will use the default output of electricity use, and the default of no constraints."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = [insulation_param] + more_parameters + [window_to_wall]\n",
    "problem = EPProblem(inputs=parameters)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sampling\n",
    "Once you have defined your parameters, you may want to generate some random possible buildings. Sampling functions allow you to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Insulation Thickness</th>\n",
       "      <th>U-Factor</th>\n",
       "      <th>Solar Heat Gain Coefficient</th>\n",
       "      <th>Window to Wall Ratio</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.982572</td>\n",
       "      <td>2.633129</td>\n",
       "      <td>0.375436</td>\n",
       "      <td>0.169052</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.043136</td>\n",
       "      <td>4.444301</td>\n",
       "      <td>0.799955</td>\n",
       "      <td>0.528535</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.497431</td>\n",
       "      <td>0.314471</td>\n",
       "      <td>0.256706</td>\n",
       "      <td>0.723646</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Insulation Thickness  U-Factor  Solar Heat Gain Coefficient  \\\n",
       "0              0.982572  2.633129                     0.375436   \n",
       "1              0.043136  4.444301                     0.799955   \n",
       "2              0.497431  0.314471                     0.256706   \n",
       "\n",
       "   Window to Wall Ratio  \n",
       "0              0.169052  \n",
       "1              0.528535  \n",
       "2              0.723646  "
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples = sampling.dist_sampler(sampling.lhs, problem, num_samples=3, criterion='maximin')\n",
    "# arguments can be passed to the specific sampler, here criterion is optional\n",
    "samples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation\n",
    "Now we can evaluate the samples. We create an energy plus evaluator (`EvaluatroEP`) using the parameters, and idf describing the building, and the objectives we want to measure. For this example we will just use one of the premade objectives: Electricity use for the whole facility."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "evaluator = EvaluatorEP(problem, building)\n",
    "# The evaluator will output electricity use by default\n",
    "outputs = evaluator.df_apply(samples ,keep_input=True)\n",
    "# outputs is a pandas dataframe with one new column since only one objective was requested"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Insulation Thickness</th>\n",
       "      <th>U-Factor</th>\n",
       "      <th>Solar Heat Gain Coefficient</th>\n",
       "      <th>Window to Wall Ratio</th>\n",
       "      <th>Electricity:Facility</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.982572</td>\n",
       "      <td>2.633129</td>\n",
       "      <td>0.375436</td>\n",
       "      <td>0.169052</td>\n",
       "      <td>1.742530e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.043136</td>\n",
       "      <td>4.444301</td>\n",
       "      <td>0.799955</td>\n",
       "      <td>0.528535</td>\n",
       "      <td>1.822360e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.497431</td>\n",
       "      <td>0.314471</td>\n",
       "      <td>0.256706</td>\n",
       "      <td>0.723646</td>\n",
       "      <td>1.751091e+09</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Insulation Thickness  U-Factor  Solar Heat Gain Coefficient  \\\n",
       "0              0.982572  2.633129                     0.375436   \n",
       "1              0.043136  4.444301                     0.799955   \n",
       "2              0.497431  0.314471                     0.256706   \n",
       "\n",
       "   Window to Wall Ratio  Electricity:Facility  \n",
       "0              0.169052          1.742530e+09  \n",
       "1              0.528535          1.822360e+09  \n",
       "2              0.723646          1.751091e+09  "
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "outputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:light",
   "text_representation": {
    "extension": ".py",
    "format_name": "light",
    "format_version": "1.3",
    "jupytext_version": "0.8.6"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
