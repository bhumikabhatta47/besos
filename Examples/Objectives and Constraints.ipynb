{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two types of outputs are supported: Objectives and Constraints. These are both made using the `MeterReader` and `VariableReader` classes. The only difference is how they are used by the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from besos import eppy_funcs as ef\n",
    "from besos import sampling\n",
    "from besos.evaluator import EvaluatorEP\n",
    "from besos.parameters import RangeParameter, wwr, FieldSelector, Parameter\n",
    "from besos.objectives import MeterReader, VariableReader, clear_outputs\n",
    "from besos.problem import EPProblem, Problem\n",
    "from besos.optimizer import NSGAII\n",
    "\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "building = ef.get_building()\n",
    "clear_outputs(building)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Objectives and constraints can be specified in one of 3 ways. The most direct is by calling their constructor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "EPProblem(outputs=[MeterReader(class_name='Output:Meter', frequency='Hourly', func=<function sum_values at 0x000001B428EDABF8>, key_name='Electricity:Facility')], minimize_outputs=[True], converters={'outputs': <class 'objectives.MeterReader'>, 'constraints': <class 'objectives.MeterReader'>})"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "objectives = [MeterReader(key_name='Electricity:Facility', class_name='Output:Meter', frequency='Hourly')]\n",
    "EPProblem(outputs=objectives)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The constructor has defaults, so we can often ommit `class_name` and `frequency`. Since the key name is often all that is needed, we can just use a list of the `key_names`. This list will be automatically be converted by `EPProblem`. Meters and variables that do not have a `frequency` specified will default to any frequency that is already used for that output, or if none is used yet then they will use Hourly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "EPProblem(outputs=[MeterReader(class_name='Output:Meter', func=<function sum_values at 0x000001B428A3B1E0>, key_name='Electricity:Facility')], minimize_outputs=[True], converters={'outputs': <class 'objectives.MeterReader'>, 'constraints': <class 'objectives.MeterReader'>})"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "objectives = ['Electricity:Facility']\n",
    "EPProblem(outputs=objectives)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we do not need the output-reading features of meters, we can use `Problem` instead of `EPProblem`, and they will be converted to `Objective` objects which act as placeholders. `EPProblem` converts them to `Meter:Reader` objects. Either of these conversions can be overriden using the converters argument.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Problem(outputs=[Objective(name='any'), Objective(name='names'), Objective(name='work')], minimize_outputs=[True, True, True], converters={'outputs': <class 'IO_Objects.Objective'>, 'constraints': <class 'IO_Objects.Objective'>})"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "objectives = ['any', 'names', 'work']\n",
    "Problem(outputs=objectives)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `func` argument to objectives/constraints is used to aggrgate the individual measurements from the model. By default, all measurements are added together. If we wanted to instead minimize the variance, we would need to write our own aggrgation function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def variance(result):\n",
    "    return result.data['Value'].var()\n",
    "\n",
    "objectives = [MeterReader('Electricity:Facility', name='Electricity Usage'),\n",
    "              MeterReader('Electricity:Facility',func=variance, name='Electricity Variance')\n",
    "             ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we want to specify the direction of optimisation, we can use `minmize_outputs`. The default is true for all objectives. Here we say we want to search for a design that has low but highly variable electricity use, and no more than 800 kg of CO2 emission. (The electricity use objectives were defined in the cell above)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs = [wwr(), Parameter(FieldSelector(class_name='Material',\n",
    "                                         object_name='Mass NonRes Wall Insulation',\n",
    "                                         field_name='Thickness'),\n",
    "                           RangeParameter(0.01, 0.99))\n",
    "         ]\n",
    "\n",
    "evaluator = EvaluatorEP(EPProblem(inputs=inputs,\n",
    "                                  outputs=objectives, minimize_outputs=[True, False],\n",
    "                                  constraints=['CO2:Facility'], constraint_bounds=['<=800']),\n",
    "                        building, out_dir='../E_Plus_Files/')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Window to Wall Ratio</th>\n",
       "      <th>Thickness</th>\n",
       "      <th>Electricity Usage</th>\n",
       "      <th>Electricity Variance</th>\n",
       "      <th>CO2:Facility</th>\n",
       "      <th>violation</th>\n",
       "      <th>pareto-optimal</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.484976</td>\n",
       "      <td>0.953711</td>\n",
       "      <td>1.742832e+09</td>\n",
       "      <td>6.980863e+14</td>\n",
       "      <td>684.017107</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.750573</td>\n",
       "      <td>0.010873</td>\n",
       "      <td>1.831939e+09</td>\n",
       "      <td>7.610804e+14</td>\n",
       "      <td>782.469859</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.653981</td>\n",
       "      <td>0.018834</td>\n",
       "      <td>1.820168e+09</td>\n",
       "      <td>7.499318e+14</td>\n",
       "      <td>765.772570</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.678340</td>\n",
       "      <td>0.034007</td>\n",
       "      <td>1.805487e+09</td>\n",
       "      <td>7.371168e+14</td>\n",
       "      <td>745.422948</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.714390</td>\n",
       "      <td>0.233137</td>\n",
       "      <td>1.765408e+09</td>\n",
       "      <td>7.090530e+14</td>\n",
       "      <td>698.479438</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>0.692106</td>\n",
       "      <td>0.047617</td>\n",
       "      <td>1.797239e+09</td>\n",
       "      <td>7.304373e+14</td>\n",
       "      <td>734.519894</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>0.819199</td>\n",
       "      <td>0.440555</td>\n",
       "      <td>1.756045e+09</td>\n",
       "      <td>7.043006e+14</td>\n",
       "      <td>691.320738</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>0.094171</td>\n",
       "      <td>0.141681</td>\n",
       "      <td>1.774404e+09</td>\n",
       "      <td>7.137429e+14</td>\n",
       "      <td>707.656368</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>0.620168</td>\n",
       "      <td>0.024005</td>\n",
       "      <td>1.814076e+09</td>\n",
       "      <td>7.446074e+14</td>\n",
       "      <td>757.632137</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>0.245806</td>\n",
       "      <td>0.636394</td>\n",
       "      <td>1.750521e+09</td>\n",
       "      <td>7.025731e+14</td>\n",
       "      <td>688.160619</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>0.951267</td>\n",
       "      <td>0.021329</td>\n",
       "      <td>1.817059e+09</td>\n",
       "      <td>7.471767e+14</td>\n",
       "      <td>761.622487</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>0.951267</td>\n",
       "      <td>0.031313</td>\n",
       "      <td>1.807812e+09</td>\n",
       "      <td>7.389946e+14</td>\n",
       "      <td>748.280988</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>0.957322</td>\n",
       "      <td>0.063231</td>\n",
       "      <td>1.790592e+09</td>\n",
       "      <td>7.252803e+14</td>\n",
       "      <td>726.049111</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>0.195686</td>\n",
       "      <td>0.084122</td>\n",
       "      <td>1.784648e+09</td>\n",
       "      <td>7.206531e+14</td>\n",
       "      <td>718.483756</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>0.355431</td>\n",
       "      <td>0.307153</td>\n",
       "      <td>1.761077e+09</td>\n",
       "      <td>7.067073e+14</td>\n",
       "      <td>694.998967</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>0.748191</td>\n",
       "      <td>0.055230</td>\n",
       "      <td>1.793549e+09</td>\n",
       "      <td>7.275457e+14</td>\n",
       "      <td>729.934173</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>0.884165</td>\n",
       "      <td>0.703847</td>\n",
       "      <td>1.747380e+09</td>\n",
       "      <td>7.003622e+14</td>\n",
       "      <td>686.435101</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17</th>\n",
       "      <td>0.541099</td>\n",
       "      <td>0.028017</td>\n",
       "      <td>1.810768e+09</td>\n",
       "      <td>7.415537e+14</td>\n",
       "      <td>752.474429</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>18</th>\n",
       "      <td>0.106290</td>\n",
       "      <td>0.073139</td>\n",
       "      <td>1.787540e+09</td>\n",
       "      <td>7.228829e+14</td>\n",
       "      <td>722.085348</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>19</th>\n",
       "      <td>0.472201</td>\n",
       "      <td>0.297089</td>\n",
       "      <td>1.761591e+09</td>\n",
       "      <td>7.070176e+14</td>\n",
       "      <td>695.396856</td>\n",
       "      <td>0</td>\n",
       "      <td>True</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "    Window to Wall Ratio  Thickness  Electricity Usage  Electricity Variance  \\\n",
       "0               0.484976   0.953711       1.742832e+09          6.980863e+14   \n",
       "1               0.750573   0.010873       1.831939e+09          7.610804e+14   \n",
       "2               0.653981   0.018834       1.820168e+09          7.499318e+14   \n",
       "3               0.678340   0.034007       1.805487e+09          7.371168e+14   \n",
       "4               0.714390   0.233137       1.765408e+09          7.090530e+14   \n",
       "5               0.692106   0.047617       1.797239e+09          7.304373e+14   \n",
       "6               0.819199   0.440555       1.756045e+09          7.043006e+14   \n",
       "7               0.094171   0.141681       1.774404e+09          7.137429e+14   \n",
       "8               0.620168   0.024005       1.814076e+09          7.446074e+14   \n",
       "9               0.245806   0.636394       1.750521e+09          7.025731e+14   \n",
       "10              0.951267   0.021329       1.817059e+09          7.471767e+14   \n",
       "11              0.951267   0.031313       1.807812e+09          7.389946e+14   \n",
       "12              0.957322   0.063231       1.790592e+09          7.252803e+14   \n",
       "13              0.195686   0.084122       1.784648e+09          7.206531e+14   \n",
       "14              0.355431   0.307153       1.761077e+09          7.067073e+14   \n",
       "15              0.748191   0.055230       1.793549e+09          7.275457e+14   \n",
       "16              0.884165   0.703847       1.747380e+09          7.003622e+14   \n",
       "17              0.541099   0.028017       1.810768e+09          7.415537e+14   \n",
       "18              0.106290   0.073139       1.787540e+09          7.228829e+14   \n",
       "19              0.472201   0.297089       1.761591e+09          7.070176e+14   \n",
       "\n",
       "    CO2:Facility  violation  pareto-optimal  \n",
       "0     684.017107          0            True  \n",
       "1     782.469859          0            True  \n",
       "2     765.772570          0            True  \n",
       "3     745.422948          0            True  \n",
       "4     698.479438          0            True  \n",
       "5     734.519894          0            True  \n",
       "6     691.320738          0            True  \n",
       "7     707.656368          0            True  \n",
       "8     757.632137          0            True  \n",
       "9     688.160619          0            True  \n",
       "10    761.622487          0            True  \n",
       "11    748.280988          0            True  \n",
       "12    726.049111          0            True  \n",
       "13    718.483756          0            True  \n",
       "14    694.998967          0            True  \n",
       "15    729.934173          0            True  \n",
       "16    686.435101          0            True  \n",
       "17    752.474429          0            True  \n",
       "18    722.085348          0            True  \n",
       "19    695.396856          0            True  "
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# this cell runs the optimisation, and may take a while.\n",
    "NSGAII(evaluator, evaluations=100, population_size=20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:light",
   "text_representation": {
    "extension": ".py",
    "format_name": "light",
    "format_version": "1.3",
    "jupytext_version": "0.8.6"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
