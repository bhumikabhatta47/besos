{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from besos import eppy_funcs as ef\n",
    "from besos import sampling\n",
    "from besos.evaluator import EvaluatorEP\n",
    "from besos.parameters import FieldSelector, FilterSelector, GenericSelector, Parameter\n",
    "from besos.problem import EPProblem\n",
    "\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "building = ef.get_building(mode='json')\n",
    "# we need the json example file because of how the insulation_filter function works"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Selectors identify which part of the building to modify, and how to modify it.\n",
    "`FieldSelector`s allow for modifying individual fields in a building.  \n",
    "\n",
    "The example building loaded above contains a `Material` class object named `Mass NonRes Wall Insulation` which has a `Thickness` field. Below is how to make a selector that modifies this insulation's thickness."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "FieldSelector(field_name='Thickness', class_name='Material', object_name='Mass NonRes Wall Insulation')"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "FieldSelector(class_name='Material', object_name='Mass NonRes Wall Insulation', field_name='Thickness')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, there is only one object in the example building with the name `Mass NonRes Wall Insulation`, so we can ommit the `class_name`. The building will be searched for any object with the correct `object_name`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "FieldSelector(field_name='Thickness', object_name='Mass NonRes Wall Insulation')"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "FieldSelector(object_name='Mass NonRes Wall Insulation', field_name='Thickness')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " If the `class_name` is provided, but the `object_name` is omitted, then the first object with that `class_name` will be used. Since JSON files do not guarentee ordering, this only works for idf files. `field_name` is mandatory."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`FilterSelectors` allow us to use custom function to select the objects to modify. Here we define a function that finds all materials with `Insulation` in their name. Then we use this function to modify the thickness of all materials in the building."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "FilterSelector(field_name='Thickness', get_objects=<function insulation_filter at 0x000002B2C09C9F28>)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def insulation_filter(building):\n",
    "    return [obj for name, obj in building['Material'].items() if 'Insulation' in name]\n",
    "\n",
    "insulation = FilterSelector(insulation_filter, 'Thickness')\n",
    "insulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When you have multiple objects of the same type that all share the same field, and want to vary that field's value, you can set the `object_name` to `'*'`, and the selector will modify all of those objects at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "FieldSelector(field_name='Watts per Zone Floor Area', class_name='Lights', object_name='*')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "lights = FieldSelector(class_name='Lights', object_name='*', field_name='Watts per Zone Floor Area')\n",
    "lights"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parameters can also be created by defining a function that takes an idf and a value and mutates the idf. These functions can be specific to a certain idf's format, and can perform any arbitrary transformation. Creating these can be more involved.  \n",
    "`eppy_funcs` contains the functions `one_window` and `wwr_all`. `one_window` removes windows from a building untill it has only one per wall. `wwr_all` takes a building with one window per wall and adjusts it to have a specific window to wall ratio."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "GenericSelector(set=<function wwr_all at 0x000002B2BDD8BB70>, setup=<function one_window at 0x000002B2BDD8BC80>)"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "window_to_wall = GenericSelector(set=ef.wwr_all, setup=ef.one_window)\n",
    "window_to_wall"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sampling\n",
    "Since Selectors do not describe the values they can take on, only where those values go, they are not sufficient for sampling. We can specify our samples manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Thickness</th>\n",
       "      <th>Watts</th>\n",
       "      <th>wwr</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.1</td>\n",
       "      <td>8</td>\n",
       "      <td>0.25</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.2</td>\n",
       "      <td>10</td>\n",
       "      <td>0.50</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.3</td>\n",
       "      <td>12</td>\n",
       "      <td>0.25</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.4</td>\n",
       "      <td>8</td>\n",
       "      <td>0.50</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.5</td>\n",
       "      <td>10</td>\n",
       "      <td>0.25</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   Thickness  Watts   wwr\n",
       "0        0.1      8  0.25\n",
       "1        0.2     10  0.50\n",
       "2        0.3     12  0.25\n",
       "3        0.4      8  0.50\n",
       "4        0.5     10  0.25"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples = pd.DataFrame({\n",
    "    'Thickness': [x/10 for x in range(1,10)]*2,\n",
    "    'Watts': [8,10,12]*6,\n",
    "    'wwr': [0.25, 0.5]*9\n",
    "})\n",
    "samples.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to put the selectors in parameters before we can use them in an evaluator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters= [Parameter(selector=x) for x in(insulation, lights, window_to_wall)]\n",
    "problem = EPProblem(inputs=parameters)\n",
    "\n",
    "evaluator = EvaluatorEP(problem, building)\n",
    "# The evaluator will use this objective by default\n",
    "outputs = evaluator.df_apply(samples ,keep_input=True)\n",
    "# outputs is a pandas dataframe with one column since only one objective was requested"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Thickness</th>\n",
       "      <th>Watts</th>\n",
       "      <th>wwr</th>\n",
       "      <th>Electricity:Facility</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.1</td>\n",
       "      <td>8</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.605449e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.2</td>\n",
       "      <td>10</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.720154e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.3</td>\n",
       "      <td>12</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.836143e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0.4</td>\n",
       "      <td>8</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.544536e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0.5</td>\n",
       "      <td>10</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.676760e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>0.6</td>\n",
       "      <td>12</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.803648e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>0.7</td>\n",
       "      <td>8</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.518209e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>0.8</td>\n",
       "      <td>10</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.655640e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>0.9</td>\n",
       "      <td>12</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.783915e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>0.1</td>\n",
       "      <td>8</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.605449e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>10</th>\n",
       "      <td>0.2</td>\n",
       "      <td>10</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.720154e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>11</th>\n",
       "      <td>0.3</td>\n",
       "      <td>12</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.836143e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>12</th>\n",
       "      <td>0.4</td>\n",
       "      <td>8</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.544536e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>13</th>\n",
       "      <td>0.5</td>\n",
       "      <td>10</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.676760e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>14</th>\n",
       "      <td>0.6</td>\n",
       "      <td>12</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.803648e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>15</th>\n",
       "      <td>0.7</td>\n",
       "      <td>8</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.518209e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>16</th>\n",
       "      <td>0.8</td>\n",
       "      <td>10</td>\n",
       "      <td>0.25</td>\n",
       "      <td>1.655640e+09</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>17</th>\n",
       "      <td>0.9</td>\n",
       "      <td>12</td>\n",
       "      <td>0.50</td>\n",
       "      <td>1.783915e+09</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "    Thickness  Watts   wwr  Electricity:Facility\n",
       "0         0.1      8  0.25          1.605449e+09\n",
       "1         0.2     10  0.50          1.720154e+09\n",
       "2         0.3     12  0.25          1.836143e+09\n",
       "3         0.4      8  0.50          1.544536e+09\n",
       "4         0.5     10  0.25          1.676760e+09\n",
       "5         0.6     12  0.50          1.803648e+09\n",
       "6         0.7      8  0.25          1.518209e+09\n",
       "7         0.8     10  0.50          1.655640e+09\n",
       "8         0.9     12  0.25          1.783915e+09\n",
       "9         0.1      8  0.50          1.605449e+09\n",
       "10        0.2     10  0.25          1.720154e+09\n",
       "11        0.3     12  0.50          1.836143e+09\n",
       "12        0.4      8  0.25          1.544536e+09\n",
       "13        0.5     10  0.50          1.676760e+09\n",
       "14        0.6     12  0.25          1.803648e+09\n",
       "15        0.7      8  0.50          1.518209e+09\n",
       "16        0.8     10  0.25          1.655640e+09\n",
       "17        0.9     12  0.50          1.783915e+09"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "outputs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:light",
   "text_representation": {
    "extension": ".py",
    "format_name": "light",
    "format_version": "1.3",
    "jupytext_version": "0.8.6"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
