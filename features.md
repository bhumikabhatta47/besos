The links in this document are to example notebooks which cover those features.
There is some overlap between the examples
# Primary Classes
## Evaluators
Evaluators handle how inputs are evaluated.  

All [Evaluators](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Evaluators.ipynb):
- Takes a list of values parameterising a building 
and return objective/constraint results
- Evaluates each row in a DataFrame and return 
the output in another DataFrame (optionally including the input)
- Caches results
- [Error handling](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Automtic%20Error%20Handling.ipynb):
    - Failfast: Stop evaluating as soon as an error is encountered
    - Print: Display errors, substitute in an error value
    - Silent: Substitute in an error value
    - Error value can be user supplied or determined automatically
    in some situations
    - Lists the input values associated with the error
- Can convert to a platypus Problem that is ready to be
optimised over

EvaluatorEP:  
- Runs EnergyPlus simulations
- Clears cache when weather or building file changed
- Manages simulation output files:
    - By default files are stored in a temporary directory,
    allowing simultaneous runs
    - Transfers files to a more accessible error directory if
    the simulation fails
    - User specified locations can also be used

EvaluatorSR:
- Evaluates a user-specified function at the input values
- Can be useful for quick debugging

[AdaptiveSR](https://gitlab.com/energyincities/EPlusGA/blob/master/Examples/Adaptive%20Surrogate%20Subclass.ipynb):
- Wraps a user specified model training process
- Evaluates the current model
- Retrains the model on new data
- Records training data
- Clears cache when retrained
- Has a reference evaluator used as a ground-truth
- Optional:
    - Finds the best points to add to the model
    - Update the model without fully retraining
- TODO: make a version that can wrap a scikit-learn pipeline
to reduce boilerplate
- TODO: make a version that can bundle multiple single objective models
together

## Problems
Problems track what inputs are valid, and how to
 apply those inputs to a building.  

Problem:
- Bundles inputs, outputs and constraints together.
- Tracks constraint bounds
- Automatically converts certain shortcut notation into
inputs/outputs/constraints
    - Strings become name only Parameters
    - Integers become that many numbered Parameters
- Gives access to names of all parts of the problem
- Resolves duplicate names
- Converts numpy arrays to a DataFrame matching the
format of the problem or some combination pieces of the
format (ex: only inputs and constraints)
- Can convert to a platypus Problem (which lacks an
evaluation function)

EPProblem:
- All the features of Problem
- Uses EnergyPlus specific converters
    - Strings for objectives/constraints become a `MeterReader` for
    the meter with that name
    - Integers still become numbered Parameters
    
Mostly covered [here](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Objectives%20and%20Constraints.ipynb)

## Parameters
Parameters track the valid inputs and building modification
steps for a single input.

[Parameter](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Creating%20and%20evaluating%20Parameters.ipynb):
- Combines a `Selector` and a `Descriptor`. (Both optional)
- Tracks the column name
- Gives access to the Selector and Descriptor's functionality.

### Pre-built parameters:
[wwr](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Selectors.ipynb):
- adjusts the Window to Wall ratio of the building.
- min/max ratios can be configured
- Assumes at least one window per exterior wall.
- Removes excess windows and centers window on walls

## Selectors
Selectors handle building modification steps for a single input.  

All [Selectors](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Selectors.ipynb):
- Correspond to some attribute of a building
- Allow setting/getting that attribute
- Can modify the building in a setup step if needed

FieldSelector:
- Corresponds to some fields in the EnergyPlus representation
of a building
- Selects objects based on class and object names
- Does not require a setup step

FilterSelector:
- Corresponds to some fields in the EnergyPlus representation
of a building
- Selects objects based on a user-defined selection function
- Does not require a setup step

DummySelector:
- A placeholder for when the selector was omitted
- Setting does nothing, getting is invalid

GenericSelector:
- Uses user-defined set/get/setup methods

## Descriptors
Descriptors describe the possible values for a single input

All [Descriptors](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Descriptors.ipynb):
- Track their equivalent platypus type
- Check if inputs are valid
- Convert a value between 0 and 1 inclusive into a valid
value for that descriptor

RangeParameter:
- Represents a real number from a continuous interval

CategoryParameter:
- Represents a value from a list

AnyValue:
- A placeholder for when the descriptor was omitted
- Accepts any value
- Cannot convert a value between 0 and 1 to an input, since all inputs
are valid

## Objectives and Constraints
Objectives and Constraints represent an attribute of the building
that we want to measure.  
Objectives are for values that we only want to measure, or that we
want to minimize/maximize.  
Constraints are for values that we want to keep within certain bounds.   
The direction of optimisation for Objectives and the bounds for 
constraints are specified in the problem.

[Objectives/Constraints](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Objectives%20and%20Constraints.ipynb):
- Retrieve their current value from the results of an EnergyPlus
simulation
- Verify that the corresponding output objects are in the building 
and add them if they are missing
- Aggregate the outputs using a user-provided function

MeterReader:
- Reads the output of Meters

VariableReader:
- Read the output of Variables

# optimizers
A simplified connector between platypus and Evaluators
- Has ready-to-go wrappers for platypus optimisation algorithms
    - NSGAII and NSGAIII
    - EpsMOEA
    - GDE3
    - SPEA2
    - ParticleSwarm
    - and more
- Automatically generates function signatures with both the Platypus
and EnergyPlus related arguments
- Can convert platypus solutions to lists of their values and 
vice versa
- Can convert lists of platypus solutions to DataFrames and
vice-versa
- Can be stopped and restarted mid-run ([example](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Optimisation%20Run%20Flexibility.ipynb))
- [Simple use case](https://gitlab.com/energyincities/EPlusGA/blob/master/Examples/Genetic%20algorithm.ipynb)
- [Example](https://gitlab.com/energyincities/EPlusGA/blob/master/Examples/Genetic%20algorithm-SR.ipynb)
with calls to all the algorithms

# sampling
Generates datapoints from the solution space
- Supports latin hypercube, random, and seeded random sampling.
- Can generate extreme values/corners of design space
- [Uses descriptors](https://gitlab.com/energyincities/EPlusGA/blob/master/Polished%20Examples/Descriptors.ipynb)
 to generate valid inputs


# eppy_funcs
- Initializes buildings (idf or json)
- Window adjustment helper functions
- Variable name conversions

