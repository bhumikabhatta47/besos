*********************************
Welcome to BESOS's documentation!
*********************************

If you want to get started on the BESOS platform, check out how to get started `here! <https://gitlab.com/energyincities/besos/blob/master/Getting%20Started%20on%20BESOS%20and%20making%20%20your%20first%20Notebook.pdf>`_

To view the PyEHub documentation go to: `<https://python-ehub.readthedocs.io>`_

.. toctree::
   :maxdepth: 2
   :caption: BESOS

	 Eppy_funcs <eppy_funcs>
	 EppySupport <eppySupport>
	 Evaluator <evaluator>
	 IO_Objects <IO_Objects>
	 Objectives <objectives>
	 Optimizer <optimizer>
	 Parameters <parameters>
	 Problem <problem>
	 Sampling <sampling>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
